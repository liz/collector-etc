#SingleInstance Force

Gui, Color, D8DEE9
Gui, Font, s18, Arial
ButtonSize := 100
Margins := 5
Gui, Add, Button, x%Margins% y%Margins% w%ButtonSize% h%ButtonSize% gqqpyp, Alice
Gui, Add, Button, x+%Margins% yp w%ButtonSize% h%ButtonSize% gnymez, Abby
Gui, Add, Button, x+%Margins% yp w%ButtonSize% h%ButtonSize% gjevyy, Astoria
Gui, Add, Button, x%Margins% y+%Margins% w%ButtonSize% h%ButtonSize% giirsl, Belle
Gui, Add, Button, x+%Margins% yp w%ButtonSize% h%ButtonSize% gsphkc, Victoria
Gui, Add, Button, x+%Margins% yp w%ButtonSize% h%ButtonSize% gojhto, Soupy
GUIHeight := 3*Margins + 2*ButtonSize
GUIWidth := 4*Margins + 3*ButtonSize
Gui, Show, w%GUIWidth% h%GUIHeight%, Cray Switcher 3000
Return

Switcher(id)
{
    reqlf := ComObjCreate("WinHttp.WinHttpRequest.5.1")
    reqlf.open("POST", "https://plural.cray.gay/", 0)
    reqlf.setRequestHeader("Content-Type", "application/json")
    reqlf.send("{""members"":[""" id """],""token"":""API_KEY_HERE"",""debug"":false}")
    reqlf.WaitForResponse()
    ; return reqlf.ResponseText
    ExitApp
}

; [nymez] Abby
nymez:
  Switcher("nymez")
Return

; [jevyy] Astoria
jevyy:
  Switcher("jevyy")
Return

; [iirsl] Belle
iirsl:
  Switcher("iirsl")
Return

; [qqpyp] Alice
qqpyp:
  Switcher("qqpyp")
Return

; [ojhto] Soup
ojhto:
  Switcher("ojhto")
Return

; [sphkc] Victoria
sphkc:
  Switcher("sphkc")
Return

Esc::
  ExitApp
Return

Ctrl & C::
  ExitApp
Return

GuiClose:
  ExitApp
Return
